// -*- Mode: vala; indent-tabs-mode: nil; tab-width: 4 -*-
/*-
* Copyright (c) 2016-2017 elementary LLC. (https://elementary.io)
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* Authored by: Nathan Dyer <mail@nathandyer.me>
*              Dane Henson <thegreatdane@gmail.com>
*/

using AppCenterCore;

const int NUM_PACKAGES_IN_BANNER = 5;
const int NUM_PACKAGES_IN_CAROUSEL = 5;

namespace AppCenter {
    public class Homepage : View {
        private Gtk.FlowBox category_flow;
        private Gtk.ScrolledWindow category_scrolled;
        private string current_category;

        public signal void page_loaded ();

        public bool viewing_package { get; private set; default = false; }

        public AppStream.Category currently_viewed_category;
        construct {
            category_flow = new Widgets.CategoryFlowBox ();
            category_flow.valign = Gtk.Align.START;

            var grid = new Gtk.Grid ();
            grid.margin = 12;
            grid.attach (category_flow, 0, 5, 1, 1);

            category_scrolled = new Gtk.ScrolledWindow (null, null);
            category_scrolled.add (grid);

            add (category_scrolled);

            category_flow.child_activated.connect ((child) => {
                var item = child as Widgets.CategoryItem;
                if (item != null) {
                    currently_viewed_category = item.app_category;
                    show_app_list_for_category (item.app_category);
                }
            });

            AppCenterCore.Client.get_default ().installed_apps_changed.connect (() => {
                Idle.add (() => {
                    // Clear the cached categories when the AppStream pool is updated
                    foreach (weak Gtk.Widget child in category_flow.get_children ()) {
                        if (child is Widgets.CategoryItem) {
                            var item = child as Widgets.CategoryItem;
                            var category_components = item.app_category.get_components ();
                            category_components.remove_range (0, category_components.length);
                        }
                    }

                    // Remove any old cached category list views
                    foreach (weak Gtk.Widget child in get_children ()) {
                        if (child is Views.AppListView) {
                            if (child != visible_child) {
                                child.destroy ();
                            } else {
                                // If the category list view is visible, don't delete it, just make the package list right
                                var list_view = child as Views.AppListView;
                                list_view.clear ();

                                unowned Client client = Client.get_default ();
                                var apps = client.get_applications_for_category (currently_viewed_category);
                                list_view.add_packages (apps);
                            }
                        }
                    }

                });
            });
        }

        public override void show_package (
            AppCenterCore.Package package,
            bool remember_history = true,
            Gtk.StackTransitionType transition = Gtk.StackTransitionType.SLIDE_LEFT_RIGHT
        ) {
            base.show_package (package, remember_history, transition);
            viewing_package = true;
            if (remember_history) {
                current_category = null;
                currently_viewed_category = null;
                subview_entered (_("Home"), false, "");
            }
        }

        public override void return_clicked () {
            transition_type = Gtk.StackTransitionType.SLIDE_LEFT_RIGHT;

            if (previous_package != null) {
                show_package (previous_package);
                if (current_category != null) {
                    subview_entered (current_category, false, "");
                } else {
                    subview_entered (_("Home"), false, "");
                }
            } else if (viewing_package && current_category != null) {
                set_visible_child_name (current_category);
                viewing_package = false;
                subview_entered (_("Home"), true, current_category, _("Search %s").printf (current_category));
            } else {
                set_visible_child (category_scrolled);
                viewing_package = false;
                currently_viewed_category = null;
                current_category = null;
                subview_entered (null, true);
            }
        }

        private void show_app_list_for_category (AppStream.Category category) {
            subview_entered (_("Home"), true, category.name, _("Search %s").printf (category.name));
            current_category = category.name;
            var child = get_child_by_name (category.name);
            if (child != null) {
                set_visible_child (child);
                return;
            }

            var app_list_view = new Views.AppListView ();
            app_list_view.show_all ();
            add_named (app_list_view, category.name);
            set_visible_child (app_list_view);

            app_list_view.show_app.connect ((package) => {
                viewing_package = true;
                base.show_package (package);
                subview_entered (category.name, false, "");
            });

            unowned Client client = Client.get_default ();
            var apps = client.get_applications_for_category (category);
            app_list_view.add_packages (apps);
        }
    }
}
